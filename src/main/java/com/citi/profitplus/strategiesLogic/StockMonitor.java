package com.citi.profitplus.strategiesLogic;

import java.util.HashMap;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.web.client.RestTemplate;

public class StockMonitor extends Thread implements IStockMonitor {
	// Rest template
	RestTemplate restTemplate;
	// JSON parser
	JSONParser jsonParser;
	// Ready to serve
	private boolean ready;
	// Create a list of stock data
	private Map<String, StockData> stocks;
	
	// Create the stock monitor
	public StockMonitor() {
		this.restTemplate = new RestTemplate();
		this.jsonParser = new JSONParser();
		this.stocks = new HashMap<String, StockData>();
		this.ready = false;
	}
	
	// Get list of stocks via API call
	private void generateStockList() throws ParseException {
		final String uri = "http://feed2.conygre.com/API/StockFeed/GetSymbolList";
	    JSONArray jsonStocks = (JSONArray) jsonParser.parse(this.restTemplate.getForObject(uri, String.class));
	    for (int i = 0; i < jsonStocks.size(); i++) {
	    	JSONObject stock = (JSONObject) jsonStocks.get(i);
	    	this.stocks.put((String) stock.get("Symbol"), 
	    			new StockData((Long) stock.get("SymbolID"),
	    			(String) stock.get("Symbol"), (String) stock.get("CompanyName")));
	    }
	    // The list of stocks is ready
	    this.ready = true;
	}
	
	// Common method to update prices
	public void updatePrices(String stockSymbol) throws ParseException {
		
		// Get existing information about the stock
		StockData currentStock = this.stocks.get(stockSymbol);
		
		// Fetch the last recorded period (if any)
		PeriodData lastKnownPeriod = null;
		if (currentStock.getPeriods().size() > 0)
			lastKnownPeriod = currentStock.getPeriods()
				.get(currentStock.getPeriods().size() - 1);
		
		// Fetch minimal values if we have a historical record, otherwise fetch more
		// numValues must be at least >5 so that don't miss out any prices
		int numValues = 20;
		// Fetch 1000 values when there is nothing in the list of periods at the beginning
		// numValues cannot be too big as it will slow down the API calls
		if (lastKnownPeriod == null) {
			// Find the largest period number available in the API (resets approx. 9pm?)
			String lastUri = "http://feed2.conygre.com/API/StockFeed/GetStockPricesForSymbol/" + 
						 stockSymbol + "?HowManyValues=1";
			JSONArray lastPrice = (JSONArray) jsonParser.parse(this.restTemplate.getForObject(lastUri, String.class));
			JSONObject lastPriceObj = (JSONObject) lastPrice.get(0);
			long lastAvailPeriodNumber = (long) lastPriceObj.get("periodNumber");
			// First fetch (20 prices x # of available periods, up till 50)
			numValues = 20 * Math.min(50, (int) lastAvailPeriodNumber);
		}
		
		// Fetch the prices to obtain number of periods in stock
		String uri = "http://feed2.conygre.com/API/StockFeed/GetStockPricesForSymbol/" + 
					 stockSymbol + "?HowManyValues=" + numValues;
		JSONArray jsonPrices = (JSONArray) jsonParser.parse(this.restTemplate.getForObject(uri, String.class));
			
		// Get list of prices (from earliest to latest)
		for (int i = 0; i < jsonPrices.size(); i++) {
			// Obtain new price
			JSONObject newPriceObject = (JSONObject) jsonPrices.get(i);
			// Obtain some details
			double newPrice = (double) newPriceObject.get("Price");
			long newPeriodNumber = (long) newPriceObject.get("periodNumber");
			String newTime = (String) newPriceObject.get("tradeTime");
			// Check if it belongs to an existing period
			if (lastKnownPeriod != null &&
				lastKnownPeriod.getPeriodNumber() == newPeriodNumber) {
				// Add it in to the period
				lastKnownPeriod.addPrice(newPrice, newTime);
			// Otherwise, create a new period if found new price
			} else if (lastKnownPeriod == null || 
					   lastKnownPeriod.getPeriodNumber() + 1 == newPeriodNumber) {
				lastKnownPeriod = new PeriodData(newPeriodNumber);
				// Add to list of periods for this stick
				currentStock.addPeriod(lastKnownPeriod);
				// Add the price to it
				lastKnownPeriod.addPrice(newPrice, newTime);
			}
		}
	}
	
	// Common method to access a particular stock (may be null)
	public StockData getStockData(String stockSymbol) throws ParseException {
		// WARNING: MAKE SURE DON'T CALL TOO FREQUENTLY (must refresh every 1 to 5s)
		// See example below:
		// while (true) {
		//    <some code here>
		//    StockData myStock = getStockData("ibm");
		//    try { Thread.sleep(1000); } catch (Exception ex) {}
		// }
		// myStock.getPeriods().get(i).getPrices() // 20 prices per period
		this.updatePrices(stockSymbol);
		return this.stocks.get(stockSymbol);
	}
	
	// Call this to check if the stock list is ready
	public boolean isReady() { return this.ready; }
	
	// Run the stock monitor thread
	public void run() {
		System.out.println("Stock Monitor is running..");
		try {
			// Obtain a list of stocks first
			generateStockList();
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			ex.printStackTrace();
			System.out.println("An error occured fetching price data from API.");
		}
	}
}
