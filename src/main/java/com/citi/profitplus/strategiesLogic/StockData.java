package com.citi.profitplus.strategiesLogic;


import java.util.ArrayList;
import java.util.List;

public class StockData {
	// belongs to class
	public static final int MAX_PERIODS = 720; // (20secs/period * 720 periods)/60/60 = 4 hours
	// Attributes
	private Long symbolID;
	private String symbol;
	private String companyName;
	// List of periods and each period contains 20 prices
	private List<PeriodData> periods;
	
	// Constructor
	public StockData(Long symbolID, String symbol, String companyName) {
		this.symbolID = symbolID;
		this.symbol = symbol;
		this.companyName = companyName;
		this.periods = new ArrayList<PeriodData>();
	}
	
	// Accessors
	public Long getSymbolID() { return this.symbolID; }
	public String getSymbol() { return this.symbol; }
	public String getCompanyName() { return this.companyName; }
	public List<PeriodData> getPeriods() { return this.periods; }
	
	// Mutators
	public void addPeriod(PeriodData newPeriod) { 
		this.periods.add(newPeriod);
		// Only record a maximum number of periods from the most recent one
		if (this.periods.size() > StockData.MAX_PERIODS) {
			this.periods.remove(0);
		}
	}
}
