package com.citi.profitplus.strategiesLogic;

import org.json.simple.parser.ParseException;

import com.citi.profitplus.entity.UserInput;

public interface IStrategyManager {
	public void deactivate(UserInput userInput);
	public void activate(UserInput userInput);
	public void updateUserInput(UserInput userInput);
	public void forceExit(UserInput userInput);
}
