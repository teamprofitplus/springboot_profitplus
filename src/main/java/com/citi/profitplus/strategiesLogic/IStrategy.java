package com.citi.profitplus.strategiesLogic;

public interface IStrategy {
	
	public void executeStrategy();
	
}
