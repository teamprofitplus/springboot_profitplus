package com.citi.profitplus.strategiesLogic;

import org.json.simple.parser.ParseException;

public interface IStockMonitor {
	// To update all the prices for a particular stock to latest available
	// Use this only if absolutely necessary
	public void updatePrices(String stockSymbol) throws ParseException;
	// Read the implementation first before using otherwise will have issues!
	public StockData getStockData(String stockSymbol) throws ParseException;

}
