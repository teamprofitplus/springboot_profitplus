package com.citi.profitplus.entity;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name="USERINPUTS")
public class UserInput{

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private long userInputId;
	private double accountBalance;
	private double strategyCapital;
	private boolean activate;
	private String stockSymbol;
	private double currentInvestmentAmount;
	private double multiples;
	//1-> activated, 0-> deactivated// 
	private int periodNumber;
	private LocalDateTime userInputDate;
	private int longPeriod;
	private int shortPeriod;
	private int period; // N
	private int currentStockQuantity;
	

	@ManyToOne
	@JoinColumn(name="strategy_id", nullable=false)
	@JsonManagedReference
	private Strategy strategy;
	
	@OneToMany(fetch = FetchType.EAGER)
	@JoinColumn(name="user_input_id")
	@LazyCollection(LazyCollectionOption.FALSE)
	//@JsonManagedReference
	@JsonBackReference
	private Collection<Order> order;
	
	public UserInput() {}
	
	//Strategy 1 Constructor
	public UserInput(double accountBalance, double strategyCapital,
			String stockSymbol, int longPeriod, int shortPeriod) {	
		this.accountBalance = accountBalance;
		this.strategyCapital = strategyCapital;
		this.activate = true;
		this.stockSymbol = stockSymbol;
		this.longPeriod = longPeriod;
		this.shortPeriod = shortPeriod;
		this.userInputDate = LocalDateTime.now();
	}

	//Strategy 2 Constructor
	public UserInput(double accountBalance, double strategyCapital,
			String stockSymbol, double multiples) {	
		this.accountBalance = accountBalance;
		this.strategyCapital = strategyCapital;
		this.activate = true;
		this.stockSymbol = stockSymbol;
		this.multiples = multiples;
		this.userInputDate = LocalDateTime.now();
	}
	

	public Collection<Order> getOrder(){
		return this.order;
	}
	
	public void setOrder(Collection<Order> order) {
		this.order = order;
	}
	
	public Strategy getStrategy() {
		return this.strategy;
	}
	
	public void setStrategy(Strategy strategy) {
		this.strategy = strategy;
	}
	
	public long getUserInputId() {
		return this.userInputId;
	}
	
	public double getAccountBalance() {
		return this.accountBalance;
	}
	
	public void setAccountBalance(double accountBalance) {
		this.accountBalance = accountBalance;
	}
	
	public void setStrategyCapital(double strategyCapital) {
		this.strategyCapital = strategyCapital;
	}
	
	public double getStrategyCapital() {
		return this.strategyCapital;
	}
	
	public void setActivate(boolean activate) {
		this.activate = activate;
	}
	
	public boolean isActivate() {
		return this.activate;
	}
	
	public String getStockSymbol() {
		return this.stockSymbol;
	}
	
	public void setStockSymbol(String stockSymbol) {
		this.stockSymbol = stockSymbol;
	}
	
	public void setCurrentInvestmentAmount(double currentInvestmentAmount) {
		this.currentInvestmentAmount = currentInvestmentAmount;
	}
	
	public double getCurrentInvestmentAmount() {
		return this.currentInvestmentAmount;
	}
	
	public double getMultiples() {
		return this.multiples;
	}
	
	public void setMultiples(double multiples) {
		this.multiples = multiples;
	}
	
	public int getPeriodNumber() {
		return this.periodNumber;
	}
	
	public void setPeriodNumber(int periodNumber) {
		this.periodNumber = periodNumber;
	}
	
	public int getLongPeriod() {
		return this.longPeriod;
	}
	
	public void setLongPeriod(int longPeriod) {
		this.longPeriod = longPeriod;
	}
	
	public int getShortPeriod() {
		return this.shortPeriod;
	}
	
	public void setShortPeriod(int shortPeriod) {
		this.shortPeriod = shortPeriod;
	}
	
	public int getPeriod() {
		return period;
	}

	public void setPeriod(int period) {
		this.period = period;
	}
	
	
	public LocalDateTime getUserInputDate() {
		return this.userInputDate;
	}
	
	public void setUserInputDate(LocalDateTime userInputDate) {
		this.userInputDate = userInputDate;
	}

	public int getCurrentStockQuantity() {
		return currentStockQuantity;
	}


	public void setCurrentStockQuantity(int currentStockQuantity) {
		this.currentStockQuantity = currentStockQuantity;
	}
	
	//TODO:: add in the variables
	@Override
	public String toString() {
		return "UserInput [userInputId=" + userInputId + ", " + "accountBalance="
				+ accountBalance + ", " + "strategyCapital=" + strategyCapital + ", " + "activate=" + activate + ", " + "stockSymbol=" + stockSymbol + ", " + "periodNumber="
				+ periodNumber + ", " + "userInputDate=" + userInputDate + ", " + "]";
	}


}


