package com.citi.profitplus.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="STOCKS") 
public class Stock {

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private long stockId;
	private String stockName;
	private String stockSymbol;
	private int symbolId;
	
	// Default Constructor
	public Stock() {
	}
	
	public Stock(long stockId, String stockName, String stockSymbol, int symbolId) {
		this.stockId = stockId;
		this.stockName = stockName;
		this.stockSymbol = stockSymbol;
		this.symbolId = symbolId;
	}

	// Get & Set methods

	public long getStockId() {
		return this.stockId;
	}

	public void setStockName(String stockName) {
		this.stockName = stockName;
	}

	public String getStockName() {
		return this.stockName;
	}
	
	public String getStockSymbol() {
		return this.stockSymbol;
	}
	
	public void setStockSymbol(String stockSymbol) {
		this.stockSymbol = stockSymbol;
	}
	
	public int getSymbolId() {
		return this.getSymbolId();
	}

	public void setSymbolId(int symbolId) {
		this.symbolId = symbolId;
	}
	

	@Override
	public String toString() {
		return "Status [stockId=" + stockId + ", stockName=" + stockName + ""
				+ "symbolID=" + symbolId + "]";
	}

}
