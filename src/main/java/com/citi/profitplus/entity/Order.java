package com.citi.profitplus.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name="ORDERS")  
public class Order{

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private long orderId;		//auto increament
	private boolean buyOrSellFlag; //true: buy, false: sell
	private double price;
	private int quantityToBuyOrSell;
	private LocalDateTime orderDate;
	private String status;	// status Fill or Reject
	private int numFilled;	//number of order filled
	
	
	///////////////////////////////////////////////NOT THIS
	//1 -> pending, 2->partially filled, 3 -> filled , 4 -> rejected
	///////////////////////////////////////////////
	private int periodNumber;
	
	
	@ManyToOne
	@JoinColumn(name="user_input_id", nullable=false)
	//@JsonBackReference
	@JsonManagedReference
	private UserInput userInput;
	
	public Order() {}
	
	public Order(UserInput userInput, boolean buyOrSellFlag, double price, 
			int quantityToBuyOrSell, String stockSymbol) {
		this.userInput = userInput;
		this.buyOrSellFlag = buyOrSellFlag;
		this.price = price;
		this.quantityToBuyOrSell = quantityToBuyOrSell;
		this.orderDate = LocalDateTime.now();
		this.status = "NEW";
	}
	
	public UserInput getUserInput() {
		return this.userInput;
	}
	
	public void setUserInput(UserInput userInput) {
		this.userInput = userInput;
	}
	
	public long getOrderId() {
		return this.orderId;
	}
	
	
	public double getPrice() {
		return this.price;
	}
	
	public void setPrice(double price) {
		this.price = price;
	}
	
	public boolean isBuyOrSellFlag() {
		return buyOrSellFlag;
	}

	public void setBuyOrSellFlag(boolean buyOrSellFlag) {
		this.buyOrSellFlag = buyOrSellFlag;
	}
	
	public int getQuantityToBuyOrSell() {
		return this.quantityToBuyOrSell;
	}
	
	public void setQuantityToBuyOrSell(int quantityToBuyOrSell) {
		this.quantityToBuyOrSell = quantityToBuyOrSell;
	}
	
//	public long getStatusId() {
//		return this.statusId;
//	}
//	
//	public void setStatusId(long statusId) {
//		this.statusId = statusId;
//	}
	
	///////////////////////Editted by hm, but pls change accordingly/////////////////////////
	public int getNumFilled() {
		return this.numFilled;
	}
	
	public void setNumFilled(int numFilled) {
		this.numFilled = numFilled;
	}
	///////////////////////Editted by hm, but pls change accordingly/////////////////////////
	
	public int getPeriodNumber() {
		return this.periodNumber;
	}
	
	public void setPeriodNumber(int periodNumber) {
		this.periodNumber = periodNumber;
	}
	
	public LocalDateTime getOrderDate() {
		return this.orderDate;
	}
	
	public void setOrderDate(LocalDateTime orderDate) {
		this.orderDate = orderDate;
	}
	
	//TODO::
	@Override
	public String toString() {
		return "Order [orderId=" + orderId + ", "
				+ "price=" + price  + ", "
				+ "balance=" + numFilled  + ", "
				+ "periodNumber=" + periodNumber  + ", "
				+ "orderDate=" + orderDate  + ", "
				+ "]";
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}




	
	
}
