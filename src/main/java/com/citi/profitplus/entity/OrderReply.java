package com.citi.profitplus.entity;

import java.time.LocalDateTime;
import java.util.Random;


// Object class no need create entity
public class OrderReply {
	private long orderId;
	private boolean buy;
	private double price;
	private int size;
	private String stock;
	private String whenAsDate;  //String literal converted from LocalDateTime
	private String statusResult;
	private int numFilled;
	
	public OrderReply() {}
	
	public OrderReply(long orderId,boolean buy, double price, int size, String stock) {
		this.orderId = orderId;
		this.buy = buy;
		this.price = price;
		this.size = size;
		this.stock = stock;
		this.whenAsDate = LocalDateTime.now().toString();

		String resultGenerated = generateRandomResult(size);
		int numFilledGenerated = generateNumFilled(size, resultGenerated);
		
		this.statusResult = resultGenerated;
		this.numFilled = numFilledGenerated;

	}
	
	public String generateRandomResult(int size) {
		Random r = new Random();
		if(size == 0) {
			return "REJECTED";
		}else if(size == 1) {
			int randomNum = r.nextInt(10);
			if(randomNum == 0) {
				return "REJECTED"; //10%
			}else {
				return "FILLED";  //90%
			}
		}else {
			int randomNum = r.nextInt(10);
			if (randomNum < 7) {
				return "FILLED";  //70%
			}
			else if(randomNum < 9) {
				return "PARTIALLY_FILLED";  //20%
			}
			else {
				return "REJECTED"; //10%
			}
		}
	}
	
	public int generateNumFilled(int size, String result) {
		if(result.equals("FILLED")) {
			return size;
		}else if(result.equals("REJECTED")) {
			return 0;
		}else {
			Random r = new Random();
			return r.nextInt(size-1) + 1;
		}
	}

	public boolean isBuy() {
		return buy;
	}

	public void setBuy(boolean buy) {
		this.buy = buy;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public String getStock() {
		return stock;
	}

	public void setStock(String stock) {
		this.stock = stock;
	}

	public String getWhenAsDate() {
		return whenAsDate;
	}

	public void setWhenAsDate(String whenAsDate) {
		this.whenAsDate = whenAsDate;
	}

	public String getResult() {
		return statusResult;
	}

	public void setResult(String result) {
		this.statusResult = result;
	}

	public int getNumFilled() {
		return numFilled;
	}

	public void setNumFilled(int numFilled) {
		this.numFilled = numFilled;
	}
	
	@Override
	public String toString() {
		return "OrderReply: {\"buy\":" + buy 
						+ ", \"price\":" + price 
						+ ", \"size\":" + size 
						+ ", \"stock\":" + stock
						+ ", \"whenAsDate\":" + whenAsDate
						+ ", \"result\": " + statusResult
						+ ", \"number filled\": " + numFilled
						+ ", \"order Id\": " + orderId + "}";
	}

	public long getOrderId() {
		return orderId;
	}

	public void setOrderId(long orderId) {
		this.orderId = orderId;
	}

}
