package com.citi.profitplus.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="STATUS")
public class Status {

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private long statusId;
	private String statusName;
	
	// Default Constructor
	public Status() {}
	
	public Status(long statusId, String statusName) {
		this.statusId = statusId;
		this.statusName = statusName;
	}
	
	// Get & Set methods
	
	public long getStatusId() {
		return this.statusId;
	}
	
	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}
	
	public String getStatusName() {
		return this.statusName;
	}
	
	@Override
	public boolean equals(Object other) {
		boolean result = false;
		if (other instanceof Status) {
			Status otherStatus = (Status)other;
			result = (this.statusId == otherStatus.statusId);
		}
		return result;
	}
	
	@Override 
	public int hashCode() {
		return (int)statusId;
	}
	
	@Override
	public String toString() {
		return "Status [statusId=" + statusId + ", statusName=" + statusName + "]";
	}
	
}
