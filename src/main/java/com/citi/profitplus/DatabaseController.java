package com.citi.profitplus;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.citi.profitplus.repository.*;
import com.citi.profitplus.entity.*;

@Component
public class DatabaseController {

	@Autowired
	private OrderRepository orderRepo;
	@Autowired
	private UserInputRepository userInputRepo;
	@Autowired
	private StrategyRepository strategyRepo;

	public void initDatabase() {

		initStrategies();
				
	}
	
	/********************* INIT DATABASE ******************************/
	// Initialize strategies
	private void initStrategies() {
		
		String[] strategiesStr = new String[] {"Two Moving Average","Bollinger Bands","Price Breakout"};
		Strategy newStrategy;
		
		for(int i=0;i<strategiesStr.length;i++) {
			newStrategy = new Strategy((i+1), strategiesStr[i]);
			strategyRepo.save(newStrategy);
		}
		
		System.out.printf("There are now %d strategies \n", strategyRepo.count());
	
	}

	/******************************************************************/
	
	
	/*********************** UI REQUEST *******************************/
	
	// Insert UserInput 
	public UserInput insertUserInput(UserInput newUser) {
		UserInput newUserInput = userInputRepo.save(newUser);
		//System.out.printf("There are now %d userInputs \n", userInputRepo.count());
		return newUserInput;
	}
	
	// Get the balance of the current account 
	public double getBalance() {
		
		Iterable<UserInput> result = userInputRepo.findAll();
		double accountBalance = 0;
		
		for(UserInput s: result){
			accountBalance += s.getAccountBalance();
		}
		
		return accountBalance;
	}

	// Get the balance of the current account 
	public Iterable<Order> getOrders() {
		
		Iterable<Order> result = orderRepo.findAll();
		return result;
	}
	
	// Get strategy history
	public Iterable<UserInput> getStrategyHistory() {
		return userInputRepo.findAll();
		//return userInputRepo.getStrategyHistory();
	}
	
	// Get strategy 
	public Strategy getStrategy(long strategyId) {		
		Strategy str = strategyRepo.findById(strategyId).orElse(null);
		return str;
	}
	
	
	
	/******************************************************************/
	
	/********************* STRATEGIES REQUEST *************************/
	
	// Insert Order 
	public Order insertNewOrder(Order order) {
		Order newOrder = orderRepo.save(order);
		//System.out.printf("There are now %d userInputs \n", userInputRepo.count());
		return newOrder;
	}
	
	// Get order by Id
	public Order findOrderById(long Id) {
		Order order = orderRepo.findById(Id).orElse(null);
		return order;
	}
	
	// Get userInput by Id
	public UserInput findUserInputById(long id) {
		UserInput userInput = userInputRepo.findById(id).orElse(null);
		return userInput;
	}
	
	// Update userInput for strategy, mainly for updating of balance and activate flag
	public UserInput updateUserInput(UserInput updatedUserInput) {
		UserInput userInput = userInputRepo.findById(updatedUserInput.getUserInputId()).orElse(null);
		userInput = updatedUserInput;
		userInput = userInputRepo.save(updatedUserInput);
		return userInput;
	}
	
	public UserInput updateStatus(long id) {
		UserInput userInput = userInputRepo.findById(id).orElse(null);
		boolean currentStatus = userInput.isActivate();
		if(currentStatus){
			userInput.setActivate(false);
		}else {
			userInput.setActivate(true);
		}
		userInput = userInputRepo.save(userInput);
		return userInput;
	}
	
	
	// Update Order Status
	public Order updateOrderStatus(Order order) {
		//System.out.println(order.getOrderId());
		Order newOrder = orderRepo.findById(order.getOrderId()).get();
		newOrder = order;
		newOrder = orderRepo.save(newOrder);
		return newOrder;
	}
	
	/******************************************************************/
	
	
	/*********************** TESTING PURPOSE **************************/
	
	
	public Iterable<UserInput> getAllUserInput() {
		return userInputRepo.findAll();
	}
	
	
	public Iterable<Order> getAllOrder() {
		return orderRepo.findAll();
	}
	
	/******************************************************************/
	
}
