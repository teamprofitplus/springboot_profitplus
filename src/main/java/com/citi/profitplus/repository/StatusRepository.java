package com.citi.profitplus.repository;

import java.util.List;
import com.citi.profitplus.entity.*;

import org.springframework.data.repository.CrudRepository;

public interface StatusRepository extends CrudRepository<Status,Long> {

}
