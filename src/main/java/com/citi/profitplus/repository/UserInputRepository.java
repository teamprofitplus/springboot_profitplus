package com.citi.profitplus.repository;

import com.citi.profitplus.entity.UserInput;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

public interface UserInputRepository extends CrudRepository<UserInput,Long>{

	
}
