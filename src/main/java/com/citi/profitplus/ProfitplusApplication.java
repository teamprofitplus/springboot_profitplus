package com.citi.profitplus;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.jms.support.converter.MappingJackson2MessageConverter;
import org.springframework.jms.support.converter.MessageConverter;
import org.springframework.jms.support.converter.MessageType;

import com.citi.profitplus.messaging.*;
import com.citi.profitplus.strategiesLogic.BBManager;
import com.citi.profitplus.strategiesLogic.IStrategyManager;
import com.citi.profitplus.strategiesLogic.StockMonitor;
import com.citi.profitplus.strategiesLogic.StrategyImpl;
import com.citi.profitplus.strategiesLogic.TwoMAStrategyManager;

//@SpringBootApplication(scanBasePackages = { "com.citi.profitplu.strategiesLogic", "com.citi.profitplu.repository", "com.citi.profitplu.entity", "com.citi.profitplus.messaging" })
@SpringBootApplication
public class ProfitplusApplication {

	public static StockMonitor stockMonitor = new StockMonitor();
	public static IStrategyManager twoMAManager = new TwoMAStrategyManager(stockMonitor);
	public static BBManager bbManager = new BBManager();
	public static OrderRequestSender jmsService;
	public static DatabaseController dbService;
	
	public static void main(String[] args) {

		ApplicationContext context = SpringApplication.run(ProfitplusApplication.class, args);
		StrategyImpl si = context.getBean("strategyImpl",StrategyImpl.class);
		dbService = context.getBean( DatabaseController.class);
		dbService.initDatabase();
		jmsService = context.getBean(OrderRequestSender.class);
		stockMonitor.start();
	}

	// service.initDatabase();

	/*
	 * //Test code for messaging queue TradeOrderSender sender1 =
	 * context.getBean(TradeOrderSender.class); TradeOrder o = new TradeOrder(true,
	 * 88.0, 2000, "HON"); sender1.sendTradeOrder(o);
	 * System.out.println("Order Sent!");
	 */

	// }

	@Bean
	public MessageConverter jacksonJmsMessageConverter() {
		MappingJackson2MessageConverter converter = new MappingJackson2MessageConverter();
		converter.setTargetType(MessageType.TEXT);
		converter.setTypeIdPropertyName("_id");
		return converter;
	}

}
