package com.citi.profitplus.messaging;

import java.time.LocalDateTime;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.support.JmsMessageHeaderAccessor;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import com.citi.profitplus.entity.OrderReply;
import com.citi.profitplus.entity.OrderRequest;


@Component
public class OrderRequestListener {
	
	private static Logger log = LoggerFactory.getLogger(OrderRequestListener.class);
	
	@Autowired
	private OrderReplySender replySender;
	
	@JmsListener(destination="OrderBroker")
    public void receiveOrderRequest(@Payload OrderRequest req,
									JmsMessageHeaderAccessor jmsMessageHeaderAccessor) {
	
		String correlationId = jmsMessageHeaderAccessor.getCorrelationId();
		
		//Print Order Message Details 
		LocalDateTime now = LocalDateTime.now();
        System.out.printf("***** Receive ORDER *****************************************************\n");
        System.out.println(" Timestamp: " + now);
        System.out.println(" Correlation ID: " + correlationId);
        System.out.println(" Order Received From: " + jmsMessageHeaderAccessor.getDestination());
        System.out.printf(" Order Details:   %s\n", req);
        System.out.printf("*********************************************************************************\n");
        
        //TO Send reply
	    //If there is correlation ID:
	    if(correlationId != null) {
	    	System.out.println("Planning to send reply:");
		    OrderReply reply = new OrderReply(req.getOrderId(),req.isBuy(), req.getPrice(), req.getSize(), req.getStock());
	    	replySender.sendOrderReply(reply, correlationId);
	    }else {
	    	log.warn("This trade order has NO Correlation ID. No response to be sent.");
	    }
	   
    }
}
