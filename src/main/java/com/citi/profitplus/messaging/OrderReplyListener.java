package com.citi.profitplus.messaging;

import java.time.LocalDateTime;

import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.support.JmsMessageHeaderAccessor;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import com.citi.profitplus.entity.OrderReply;


@Component
public class OrderReplyListener {
	
	@JmsListener(destination="OrderBroker_Reply")
	public void receiveOrderReply(@Payload OrderReply reply,
								  JmsMessageHeaderAccessor jmsMessageHeaderAccessor) {
		
		String correlationId = jmsMessageHeaderAccessor.getCorrelationId();
		
		//Print Reply message details 
		LocalDateTime now = LocalDateTime.now();
        System.out.printf("*****  Receive REPLY *****************************************************\n");
        System.out.println(" Timestamp: " + now);
        //System.out.printf(" Timestamp: %s %s\n", now.toLocalDate(), now.toLocalTime());
        System.out.println(" Correlation ID: " + correlationId);
        System.out.println(" Reply Received From: " + jmsMessageHeaderAccessor.getDestination());
        System.out.printf(" Reply Details:   %s\n", reply);
        System.out.printf("***************************************************************************************\n");
        
        
	}

}
