package com.citi.profitplus;

import static org.junit.Assert.*;

import org.junit.Test;

import com.citi.profitplus.strategiesLogic.PeriodData;

public class TestPeriodData {

	@Test
	public void test() {
		// Test period number 1
		PeriodData testPeriod = new PeriodData(0L);
		// Add some prices to test (1st price is $1, ..., last price is $20)
		for (Integer i = 1; i <= 20; i++) {
			// Create seconds "01" to "20"
			String seconds = i.toString();
			if (seconds.length() < 2) seconds = "0" + seconds;
			testPeriod.addPrice((double)i, "23:59:" + seconds);
		}
		// Test price average
		assertTrue(10.5 == testPeriod.getPriceAverage());
		assertTrue(1 == testPeriod.getOpeningPrice());
		assertTrue(20 == testPeriod.getClosingPrice());
		assertTrue(1 == testPeriod.getLowestPrice());
		assertTrue(20 == testPeriod.getHighestPrice());
		assertTrue("23:59:01".equals(testPeriod.getStartTime()));
		assertTrue("23:59:20".equals(testPeriod.getEndTime()));
		assertTrue(0L == testPeriod.getPeriodNumber());
		assertTrue(20 == testPeriod.getPrices().size());
	}

}
