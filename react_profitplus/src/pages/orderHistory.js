import React from 'react';
import { Grid, Paper, Typography } from '@material-ui/core';
import MUIDataTable from "mui-datatables";
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import PageTitle from '../components/PageTitle/PageTitle';
const axios = require('axios');

const green = '#12b335', red = '#ff001e', yellow = '#ffaa00';


const columns = [
    {
        name: "orderId",
        label: "Order ID",
        options: {
            filter: false,
            sort: true,
            sortDirection: 'dec'
        }
    },
    {
        name: "userInput.strategy.strategyName",
        label: "Strategy Name",
        options: {
            filter: true,
            sort: true,
        }
    },
    {
        name: "buyOrSellFlag",
        label: "Buy/Sell",
        options: {
            filter: true,
            sort: true,
            customBodyRender: (value, tableMeta, updateValue) => {
                const buyrOrSell = value ? 'BUY' : 'SELL'
                const color = value ? green : red
                return (
                    <p style={{ color: color }}>{buyrOrSell}</p>
                );
            }
        }
    },
    {
        name: "userInput.stockSymbol",
        label: "Strategy Name",
        options: {
            filter: false,
            sort: true,
        }
    },
    {
        name: "price",
        label: "Price (USD$)",
        options: {
            filter: false,
            sort: true,
        }
    },
    {
        name: "quantityToBuyOrSell",
        label: "Quantity",
        options: {
            filter: false,
            sort: true,
        }
    },
    {
        name: "numFilled",
        label: "Number Filled",
        options: {
            filter: false,
            sort: true,
        }
    },
    {
        name: "status",
        label: "Status",
        options: {
            filter: false,
            sort: true,
            customBodyRender: (value, tableMeta, updateValue) => {
                let color;
                if (value === 'FILLED') {
                    color = green
                } else if (value === 'PARTIALLY_FILLED') {
                    color = yellow
                } else {
                    color = red
                }

                return (
                    <p style={{ color: color }}>{value}</p>
                );
            }
        }
    },
    {
        name: "orderDate",
        label: "DateTime",
        options: {
            filter: false,
            sort: true,
        }
    },
];

const getMuiTheme = () => createMuiTheme({
    overrides: {
        MUIDataTableSelectCell: {
            checkboxRoot: {
                display: 'none'
            }
        }
    }
});

let intervalId;
let initTableFlag = false;
class DetailsTable extends React.Component {

    state = {
        orderHistory: [],
    };

    async componentDidMount() {
        let response = await axios.get(`${process.env.REACT_APP_API}/api/allOrder`);
        this.setState({ orderHistory: response.data });
        this.keepPullingData();
    }

    componentWillUnmount() {
        window.clearInterval(intervalId);
    }

    async keepPullingData() {
        intervalId = window.setInterval(() => {
            try {
                this.getOrderHistory();
            } catch{
                console.log("error");
            }

        }, 3000)
    }

    async getOrderHistory() {
        try {
            let response = await axios.get(`${process.env.REACT_APP_API}/api/allOrder`);
            if (this.state.orderHistory.length < response.data.length)
                this.setState({ orderHistory: response.data });
        } catch (error) {
            console.log(error)
        }
    }

    render() {
        const { orderHistory } = this.state
        return (
            <Grid>
                <PageTitle title="Order History" />
                <Grid container spacing={32}>
                    <Grid item xs={12}>
                        <MuiThemeProvider theme={getMuiTheme()}>
                            <MUIDataTable
                                data={orderHistory}
                                columns={columns}
                                options={{
                                    filterType: 'checkbox',
                                }}
                            />
                        </MuiThemeProvider>
                    </Grid>
                </Grid>
            </Grid>
        );
    }
}

export default DetailsTable;