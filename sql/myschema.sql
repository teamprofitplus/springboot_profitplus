-- Create PROFITPLUSDB schema.
 CREATE SCHEMA PROFITPLUSDB;

-- Create STRATEGIES table.
CREATE TABLE PROFITPLUSDB.STATEGIES (
	strategy_id INT NOT NULL AUTO_INCREMENT,
	strategy_name VARCHAR(50) NOT NULL,
	
	PRIMARY KEY (strategy_id)
);

-- Create STATUS table.
CREATE TABLE PROFITPLUSDB.STATUS (
	status_id INT NOT NULL AUTO_INCREMENT,
	status_name VARCHAR(50) NOT NULL,
	
	PRIMARY KEY (status_id)
);

-- Create STOCKS table. (dont need)
CREATE TABLE PROFITPLUSDB.STOCKS (
	stock_id INT NOT NULL AUTO_INCREMENT,
	stock_name VARCHAR(50) NOT NULL,
	stock_symbol VARCHAR(50) NOT NULL,
	symbol_id INT NOT NULL,
	
	PRIMARY KEY (stock_id)
);

-- Create TMAINPUTS table.
CREATE TABLE PROFITPLUSDB.TMAINPUTS (
	tma_id INT NOT NULL AUTO_INCREMENT,
	user_input_id INT NOT NULL,	
	long_period INT NOT NULL,
	short_period INT NOT NULL,
	
	PRIMARY KEY (tma_id)
);

-- Create BBINPUTS table.
CREATE TABLE PROFITPLUSDB.BBINPUTS (
	bb_id INT NOT NULL AUTO_INCREMENT,
	user_input_id INT NOT NULL,	
	period INT NOT NULL,
	profit_threshold DOUBLE NOT NULL,
	loss_threshold DOUBLE NOT NULL,
	
	PRIMARY KEY (bb_id)
);

-- Create PBINPUTS table.
CREATE TABLE PROFITPLUSDB.PBINPUTS (
	pb_id INT NOT NULL AUTO_INCREMENT,
	user_input_id INT NOT NULL,	
	period INT NOT NULL,
	
	PRIMARY KEY (pb_id)
);

-- Create USERINPUTS table.
CREATE TABLE PROFITPLUSDB.USERINPUTS (
	user_input_id INT NOT NULL AUTO_INCREMENT,
    strategy_id INT NOT NULL,
	account_balance DOUBLE,
	strategy_capital DOUBLE, 
	activate BOOLEAN NOT NULL,
	stock_id INT NOT NULL,
	period_number INT NOT NULL,
	user_input_date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY (user_input_id),
	
	CONSTRAINT Strategy_FK FOREIGN KEY (strategy_id) REFERENCES PROFITPLUSDB.STRATEGIES(StrategyID),
	CONSTRAINT Stock_FK FOREIGN KEY (stock_id) REFERENCES PROFITPLUSDB.STOCKS(stock_id), 
	CONSTRAINT Status_FK FOREIGN KEY (status_id) REFERENCES PROFITPLUSDB.STATUS(status_id)
);

-- Create ORDERS table.
CREATE TABLE PROFITPLUSDB.ORDERS (
	order_id INT NOT NULL AUTO_INCREMENT,
	user_input_id INT NOT NULL,
	price DOUBLE NOT NULL,
	balance INT NOT NULL,
	period_number INT NOT NULL,
	order_date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,	
	
	PRIMARY KEY (order_id),
	CONSTRAINT UserInput_FK FOREIGN KEY (user_input_id) REFERENCES PROFITPLUSDB.USERINPUTS(user_input_id)
);


-- Populate STRATEGIES table.
INSERT INTO PROFITPLUSDB.STRATEGIES (strategy_name) VALUES 
	('Two Moving Averages'),
	('Bollinger Bands'),
	('Price Breakout');
	
-- Populate STATUS table.
INSERT INTO PROFITPLUSDB.STATUS (status_name) VALUES 
	('Pending'),
	('Partially Filled'),
	('Filled'),
	('Rejected');	
	
